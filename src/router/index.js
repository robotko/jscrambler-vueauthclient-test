import Vue from "vue"
import Vuetify from "vuetify"
import Router from "vue-router"
import Login from "@/components/Login"
import Dashboard from "@/components/Dashboard"
import "vuetify/dist/vuetify.min.css"
Vue.use(Router)
Vue.use(Vuetify)

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Login",
      component: Login
    },
    {
      path: "/dashboard",
      name: "Dashboard",
      component: Dashboard
    }
  ]
})
